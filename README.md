# Dotfiles

## Installation

```bash
cd <project location>

STOW_DIRS=( "alacritty" "bin" "nvim" "tmux" "zsh" "kmonad" )

for f in "${STOW_DIRS[@]}"; do
  stow -R -t $HOME $f
done
```

## board Layout

https://configure.zsa.io/ergodox-ez/layouts/7Vqow/n4EGJ/0
