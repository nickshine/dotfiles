#export XDG_CONFIG_HOME=$HOME/.config
VIM="nvim"
export DOTFILES=$HOME/dev/dotfiles

export PATH="/usr/local/bin${PATH+:$PATH}"
eval "$(/opt/homebrew/bin/brew shellenv)"
# eval "$(`which brew` shellenv)"
#
export PATH="$HOMEBREW_PREFIX/opt/curl/bin:$PATH"
export PATH=$HOME/bin:$PATH
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$(npm prefix -g)/bin
# export PATH=$PATH:$HOME/.cargo/bin
export PATH=$PATH:$HOME/.asdf/installs/rust/1.67.1/bin
export PATH="/opt/homebrew/opt/curl/bin:$PATH"
export PATH="/opt/homebrew/opt/gnu-tar/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/findutils/libexec/gnubin:$PATH"


export HOMEBREW_NO_AUTO_UPDATE=1

# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh
export EDITOR=$VIM

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  ssh-agent
  #base16-shell
  asdf
  aws
  brew
  direnv
  # cloudfoundry
  # docker
  docker-compose
  fzf
  fzf-zsh-plugin
  gh
  git
  # git-open
  golang
  helm
  jfrog
  # kube-ps1
  kubectl
  # lpass
  # mvn
  node
  pip
  # python
  # redis-cli
  rust
  # spring
  sudo
  terraform
  tmux
  urltools
  vault
  vi-mode
  # vscode
  yarn
  zsh-autosuggestions
  # zsh-syntax-highlighting
)
# unused plugins:
# zsh-syntax-highlighting

export SHOW_AWS_PROMPT=false
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
# fpath=(/usr/local/share/zsh-completions $fpath)


alias vim=nvim
alias vimdiff='nvim -d'
#alias tmux="TERM=xterm-256color tmux"


alias ll='ls -lh'
alias la='ls -lha'
alias lsd='ls -l | grep "^d"'
alias c='clear'
alias cd..='cd ../'
alias ..='cd ../'
alias ...='cd ../../'

alias etree='tree -L 2 -I "node_modules|images"'
alias reload='source ~/.zshrc'
alias k='kubectl'
alias kn='kubens'
alias kc='kubectx'

#alias aws='aws --color on'
alias tf='terraform'
alias tg="terragrunt"
alias tfs="tfswitch"
alias v='vault'
export TF_PLUGIN_CACHE_DIR=$HOME/.terraform.d/plugin-cache

alias rg='rg --hidden'
alias mem-by-proc="ps aux | head -1; ps aumx"

alias grep='ggrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}'
alias sed='gsed'

alias glab='NO_COLOR=1 glab'
alias date='gdate'

# remove default run-help aliased to man
unalias run-help &> /dev/null
# use zsh's run-help
autoload run-help
HELPDIR=/usr/share/zsh/$(zsh --version | cut -f2 -d' ')/help
alias help=run-help

export HISTIGNORE="$HISTIGNORE:ls:la:ll:lsd:history:clear:pwd"

cd() { builtin cd "$@"; ls -Alh; }

export CLICOLOR=1
export LSCOLORS=Gxfxcxdxbxegedabagacad

function gcap() {
	git add .
	git commit -m "$1"
	git push
}

go_test() {
  go test $* | sed ''/PASS/s//$(printf "\033[32mPASS\033[0m")/'' | sed ''/SKIP/s//$(printf "\033[34mSKIP\033[0m")/'' | sed ''/FAIL/s//$(printf "\033[31mFAIL\033[0m")/'' | sed ''/FAIL/s//$(printf "\033[31mFAIL\033[0m")/'' | GREP_COLOR="01;33" egrep --color=always '\s*[a-zA-Z0-9\-_.]+[:][0-9]+[:]|^'
}

gen_PKCE() {
  echo "Generating PKCE Verifier"

  VERIFIER=`LC_ALL=C tr -dc 'a-zA-Z0-9' </dev/urandom | fold -w 50 | head -n 1`
  echo "Verifier is: $VERIFIER"

  #Generate PKCE Challenge from Verifier and convert / + = characters"
  CHALLENGE=`echo -n $VERIFIER | openssl dgst -binary -sha256 | base64 | tr / _ | tr + - | tr -d =`
  echo "Challenge is: $CHALLENGE"
  echo
  echo "*****************"
}

alias yaml2js="python3 -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)'"

jwt() {
  jq -R 'split(".") | .[1] | @base64d | fromjson'
}

# PROMPT='$(kube_ps1)'$PROMPT

function terraform_prompt()
{
    if [ -d .terraform ]; then
        workspace="$(command terraform workspace show 2>/dev/null)"
        echo "(${workspace}) "
    fi
}

function ss() {
    ssh -t $@ 'bash -o vi'
}

# brew install coreutils for gdate
# pass in future date like "9pm":
#
# $ dates 9pm
#
# Thu, Mar 09, 07:00:00 PM PST    US/Pacific
# Thu, Mar 09, 08:00:00 PM MST    US/Mountain
# Thu, Mar 09, 09:00:00 PM CST    US/Central
# Thu, Mar 09, 10:00:00 PM EST    US/Eastern
# Fri, Mar 10, 03:00:00 AM UTC    UTC
# Fri, Mar 10, 04:00:00 AM CET    Europe/Warsaw
# Fri, Mar 10, 02:00:00 PM AEDT   Australia/Sydney

function dates() {
    d=$(gdate -d "$1" +%s)
    for tz in US/Pacific US/Mountain US/Central US/Eastern UTC Europe/Warsaw Australia/Sydney; do
        TZ=$tz gdate -d "@$d" +"%a, %b %d, %r %Z%t$tz"
    done
}

# export PS1='\u@\h \[\033[38;5;135m\]\w\[\e[38;5;99m\]\[\e[1m\]$(terraform_prompt)\e[0m\$ '

# PROMPT=$PROMPT'$(terraform_prompt)'

# pipx completions
eval "$(register-python-argcomplete pipx)"

export AWS_PAGER=



# install fd utility for this:
export FZF_DEFAULT_COMMAND='fd --type file --follow --hidden --exclude .git --color=always'
export FZF_DEFAULT_OPTS="--ansi"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

if [ -r "${HOME}/.zshrc.local" ]; then
  source ${HOME}/.zshrc.local
fi
autoload -U compinit; compinit
source "${XDG_CONFIG_HOME:-$HOME/.config}/asdf-direnv/zshrc"
