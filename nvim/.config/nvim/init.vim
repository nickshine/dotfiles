set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
" source ~/.vimrc

set exrc
set guicursor=
set relativenumber
set number
set nohlsearch
set hidden
set noerrorbells
" set tabstop=4 softtabstop=4
set shiftwidth=4
" set expandtab
set smartindent
set nowrap
set ignorecase
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect " for nvim-cmp
set colorcolumn=80
set signcolumn=yes
set cmdheight=1
set updatetime=100
set shortmess+=c
set splitright

set listchars=tab:»\ ,extends:❯,precedes:❮,nbsp:·,trail:·
set wildmode=longest,list,full
set wildmenu
set wildignore+=**/.git/*


call plug#begin('~/.vim/plugged')

" theme
" Plug 'chriskempson/base16-vim'
Plug 'fnune/base16-vim'
" Plug 'gruvbox-community/gruvbox'

Plug 'edkolev/tmuxline.vim'
Plug 'nvim-lualine/lualine.nvim', { 'commit': '181b143' }
Plug 'kyazdani42/nvim-web-devicons'
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }


" navigation
Plug 'christoomey/vim-tmux-navigator'
Plug 'jeffkreeftmeijer/vim-numbertoggle'

Plug 'mbbill/undotree'
Plug 'numToStr/Comment.nvim'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'ThePrimeagen/harpoon'


" telescope requirements...
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-live-grep-args.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'ThePrimeagen/git-worktree.nvim'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'
Plug 'romgrk/nvim-treesitter-context'
Plug 'windwp/nvim-autopairs'

" lsp plugins
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'jay-babu/mason-nvim-dap.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/nvim-cmp'
Plug 'simrat39/symbols-outline.nvim'
Plug 'tzachar/cmp-tabnine', { 'do': './install.sh' }
Plug 'onsails/lspkind-nvim'
Plug 'simrat39/rust-tools.nvim'
Plug 'https://gitlab.com/schrieveslaach/sonarlint.nvim.git'
Plug 'simondrake/gomodifytags'


" snippets
Plug 'L3MON4D3/LuaSnip'
Plug 'rafamadriz/friendly-snippets'

Plug 'ray-x/go.nvim'
" Plug 'darrikonn/vim-gofmt'


Plug 'editorconfig/editorconfig-vim'
Plug 'sbdchd/neoformat'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'

Plug 'mhinz/vim-rfc'
Plug 'kmonad/kmonad-vim'
Plug 'google/vim-jsonnet'

" debugging
Plug 'mfussenegger/nvim-dap'
Plug 'leoluz/nvim-dap-go'
Plug 'rcarriga/nvim-dap-ui'
Plug 'mxsdev/nvim-dap-vscode-js'
Plug 'theHamsta/nvim-dap-virtual-text'
Plug 'nvim-telescope/telescope-dap.nvim'
Plug 'David-Kunz/jester'


call plug#end()

" if filereadable(expand("~/.vimrc_background"))
" 	let base16colorspace=256
" 	source ~/.vimrc_background
" endif

" let g:gruvbox_contrast_dark = "soft"
" let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
" let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
" let g:gruvbox_invert_selection = 0
" set background=dark
" set termguicolors
" colorscheme gruvbox
" highlight ColorColumn ctermbg=0 guibg=grey
" hi CursorLineNr guibg=none
" highlight Normal guibg=none
" highlight LineNr guifg=#ff8659
" highlight Normal guibg=none

set background=dark
set termguicolors
colorscheme base16-ocean
" highlight Normal guibg=none
highlight Normal guibg=bg
highlight LineNr guibg=g:terminal_color_0
highlight SignColumn guibg=g:terminal_color_0

" for chrome firenvim
set guifont=DroidSansMono\ Nerd\ Font:h20
function! OnUIEnter(event) abort
  if 'Firenvim' ==# get(get(nvim_get_chan_info(a:event.chan), 'client', {}), 'name', '')
    set laststatus=0
  endif
endfunction
autocmd UIEnter * call OnUIEnter(deepcopy(v:event))


let g:firenvim_config = { 
    \ 'globalSettings': {
        \ 'alt': 'all',
    \  },
    \ 'localSettings': {
        \ '.*': {
            \ 'cmdline': 'neovim',
            \ 'content': 'text',
            \ 'priority': 0,
            \ 'selector': 'textarea',
            \ 'takeover': 'never',
        \ },
    \ }
\ }

let fc = g:firenvim_config['localSettings']
let fc['https://[^/]+\.atlassian.net/'] = { 'takeover': 'never', 'priority': 1 }

let g:netrw_browse_split = 0
let g:netrw_banner = 0
let g:netrw_winsize = 25
" let g:netrw_keepdir = 0

let g:jsonnet_fmt_on_save = 0

" let loaded_matchparen = 1

let mapleader = " "

" all lua configs
lua require('nshine')


nmap <Leader>sr :source $MYVIMRC<CR>
nmap <Leader>vrc :tabedit $MYVIMRC<CR>
nnoremap <leader>- :vertical res-10<CR>
nnoremap <leader>= :vertical res+10<CR>
nnoremap <leader>_ :res-5<CR>
nnoremap <leader>+ :res+5<CR>
" maximize horizontal split
nnoremap <Leader>fh <C-W>_
" maximize vertical split
nnoremap <Leader>fv <C-W>\|
" rebalance splits
nnoremap <Leader>fr <C-W>=

nnoremap <leader>l :set list!<CR>
nnoremap <leader>h :set hlsearch!<CR>
map <leader>sa ggVG

nnoremap <leader>^ <C-^>

vnoremap <M-a> g<C-A>

vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
" nnoremap <leader>j :m .+1<CR>==
" nnoremap <leader>k :m .-2<CR>==

" quickfix list
nnoremap <leader>j :cnext<CR>zz
nnoremap <leader>k :cprev<CR>zz
nnoremap <leader>J :lnext<CR>zz
nnoremap <leader>K :lprev<CR>zz
nnoremap <C-q> :call ToggleQFList(1)<CR>
nnoremap <leader>q :call ToggleQFList(0)<CR>

fun! ToggleQFList(global)
  if a:global
    if empty(filter(getwininfo(), 'v:val.quickfix'))
        copen
    else
        cclose
    endif
  else
    if empty(filter(getwininfo(), 'v:val.loclist'))
      lopen
    " else
    "   lclose
    endif
  endif
endfunction

fun! LspLocationList()
    lua vim.diagnostic.setloclist({open = false})
endfun

fun! LspQuickfixList()
    lua vim.lsp.diagnostic.set_qflist()
endfun


" keep centered
nnoremap Y yg$
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z
nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz

" Undo break points
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

" add large relative jumps to jumplist
nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'

xnoremap <leader>p "_dP
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nmap <leader>Y "+y
nnoremap <leader>d "_d
vnoremap <leader>d "_d

" git
nmap <leader>gf :diffget //2<CR>
nmap <leader>gj :diffget //3<CR>
nmap <leader>gs :G<CR>


nnoremap <leader>u :UndotreeToggle<CR>
nnoremap <leader>so :SymbolsOutline<CR>
" nnoremap <leader>pv :Sex!<CR>
nnoremap <leader>pv :Lex<CR>
nmap <C-s> <Plug>MarkdownPreviewToggle

" lsp
nnoremap <leader>gd :lua vim.lsp.buf.definition()<CR>
nnoremap <leader>gi :lua vim.lsp.buf.implementation()<CR>
nnoremap <leader>gsh :lua vim.lsp.buf.signature_help()<CR>
nnoremap <leader>grr :lua vim.lsp.buf.references()<CR>
nnoremap <leader>grn :lua vim.lsp.buf.rename()<CR>
nnoremap <leader>gh :lua vim.lsp.buf.hover()<CR>
nnoremap <leader>gca :lua vim.lsp.buf.code_action()<CR>
nnoremap <leader>gsd :lua vim.diagnostic.open_float()<CR>
nnoremap <leader>dd :lua vim.diagnostic.disable()<CR>
nnoremap <leader>de :lua vim.diagnostic.enable()<CR>
nnoremap <leader>dh :lua vim.diagnostic.hide()<CR>
nnoremap <leader>ds :lua vim.diagnostic.show()<CR>
nnoremap <leader>gp :lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <leader>gn :lua vim.lsp.diagnostic.goto_next()<CR>
nnoremap <leader>gq :lua vim.diagnostic.setloclist()<CR>
nnoremap <leader>gll :call LspLocationList()<CR>


" telescope
nnoremap <leader>ps :lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep For > ")})<CR>
nnoremap <C-p> :lua require('telescope.builtin').git_files()<CR>
nnoremap <leader>pf :lua require('telescope.builtin').find_files({ hidden = true})<CR>
nnoremap <leader>pg :lua require('telescope.builtin').live_grep()<CR>
nnoremap <leader>pag :lua require('telescope').extensions.live_grep_args.live_grep_args()<CR>
nnoremap <leader>pw :lua require('telescope.builtin').grep_string { search = vim.fn.expand("<cword>") }<CR>
nnoremap <leader>pb :lua require('telescope.builtin').buffers()<CR>
nnoremap <leader>pm :lua require('telescope.builtin').marks()<CR>
nnoremap <leader>vh :lua require('telescope.builtin').help_tags()<CR>
nnoremap <leader>pd :lua require('telescope.builtin').find_files({ prompt_title = "< Dotfiles >", cwd = "~/dev/dotfiles", hidden = true, no_ignore = false })<CR>
nnoremap <leader>vm :lua require('telescope.builtin').keymaps()<CR>
nnoremap <leader>gc :lua require('telescope.builtin').git_branches()<CR>
nnoremap <leader>gw :lua require('telescope').extensions.git_worktree.git_worktrees()<CR>
nnoremap <leader>gm :lua require('telescope').extensions.git_worktree.create_git_worktree()<CR>

nnoremap <leader>tag :lua require('gomodifytags').GoAddTags("json,yaml", {transformation = "snakecase", skip_unexported = false})<CR>


" harpoon
" nnoremap <silent><leader>a :lua require("harpoon.mark").add_file()<CR>
nnoremap <leader>a :lua require("harpoon.mark").add_file()<CR>
nnoremap <leader>e :lua require("harpoon.ui").toggle_quick_menu()<CR>
nnoremap <silent><leader>1 :lua require("harpoon.ui").nav_file(1)<CR>
nnoremap <silent><leader>2 :lua require("harpoon.ui").nav_file(2)<CR>
nnoremap <silent><leader>3 :lua require("harpoon.ui").nav_file(3)<CR>
nnoremap <silent><leader>4 :lua require("harpoon.ui").nav_file(4)<CR>

" debugging
nmap <silent> <leader>td :lua require('dap-go').debug_test()<CR>
nnoremap <silent> <F5> :lua require'dap'.continue()<CR>
nnoremap <silent> <F2> :lua require'dap'.step_over()<CR>
nnoremap <silent> <F3> :lua require'dap'.step_into()<CR>
nnoremap <silent> <F4> :lua require'dap'.step_out()<CR>
nnoremap <silent> <leader>b :lua require'dap'.toggle_breakpoint()<CR>
nnoremap <silent> <leader>B :lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
nnoremap <silent> <leader>lp :lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
nnoremap <silent> <leader>dr :lua require'dap'.repl.open()<CR>
nnoremap <silent> <leader>dl :lua require'dap'.run_last()<CR>
nnoremap <silent> <leader>dm :lua require'dap'.run()<CR>
nnoremap <silent> <leader>dui :lua require'dapui'.toggle()<CR>


augroup fmt
    autocmd!
    autocmd BufWritePre *.tf,*.tfvars,*.py,*.js undojoin | Neoformat
    autocmd BufWritePre *.go silent! lua require('go.format').goimport()
augroup END

autocmd FileType go setlocal noexpandtab " needed with minimal darrikonn/vim-gofmt plugin
autocmd BufNewFile,BufRead *.md setlocal textwidth=100
autocmd BufEnter .env* lua vim.diagnostic.disable()


" autocmd CursorHold * lua vim.diagnostic.open_float(nil, { focusable = false })

augroup nlsp
    autocmd!
    autocmd! BufWrite,BufEnter,InsertLeave * :call LspLocationList()
augroup END

" https://github.com/christoomey/vim-tmux-navigator/issues/189#issuecomment-620485838
augroup netrw_mapping
  autocmd!
  autocmd FileType netrw call NetrwMapping()
augroup END

function! NetrwMapping()
  nnoremap <silent> <buffer> <c-l> :TmuxNavigateRight<CR>
endfunction
