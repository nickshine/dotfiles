require('lualine').setup()
require('nvim-web-devicons').setup { default = true }
require('nvim-treesitter.configs').setup {
	ensure_installed = "all",
	highlight = { enable = true }, -- disable highlight due to vim-help error in 0.8.1
	incremental_selection = { enable = true },
	textobjects = { enable = true }
}

require('treesitter-context').setup({
	enable = true,
	throttle = true,
	max_lines = 0,
	patterns = {
		default = {
			'class',
			'function',
			'method',
			'for',
			'while',
			'if',
			'switch',
			'case',
		},
	}
})
require('symbols-outline').setup()
require ('nvim-autopairs').setup()
require('go').setup()

-- setup debugging
require('dap-go').setup({
	dap_configurations = {
		{
			type = "go",
			name = "Attach remote",
			mode = "remote",
			request = "attach",
		},
	},
})

require('gomodifytags').setup()

require('dap-vscode-js').setup({
  debugger_path = vim.fn.stdpath("data") .. "/mason/packages/js-debug-adapter",
  debugger_cmd = { "js-debug-adapter"},
  adapters = {"pwa-node", "pwa-chrome", "pwa-msedge", "node-terminal", "pwa-extensionHost", "chrome"},
})

-- require('jester').setup({
--	   path_to_jest_run = './node_modules/.bin/jest'
-- })

require("nvim-dap-virtual-text").setup()
require("dapui").setup()

local dap, dapui = require("dap"), require("dapui")

dap.adapters.chrome = {
  type = "executable",
  command = "node",
  args = { vim.fn.stdpath("data") .. "/mason/packages/chrome-debug-adapter/out/src/chromeDebug.js" },
}

for _, language in ipairs({ "typescript", "javascript", "typescriptreact" }) do
  dap.configurations[language] = {
	  {
		type = "pwa-node",
		request = "launch",
		name = "Launch file",
		program = "${file}",
		cwd = "${workspaceFolder}",
	  },
	  {
		type = "pwa-node",
		request = "attach",
		name = "Attach",
		processId = require'dap.utils'.pick_process,
		cwd = "${workspaceFolder}",
	  },
	  {
		type = "pwa-chrome",
		name = "Attach - Remote Debugging",
		request = "attach",
		program = "${file}",
		cwd = vim.fn.getcwd(),
		sourceMaps = true,
		protocol = "inspector",
		port = 9222,
		webRoot = "${workspaceFolder}",
	  },
	  {
		type = "pwa-chrome",
		request = "launch",
		name = "Start Chrome with \"localhost\"",
		url = "http://localhost:3000",
		webRoot = "${workspaceFolder}",
		userDataDir = "${workspaceFolder}/.vscode/vscode-chrome-debug-userdatadir"
	  },
	  {
		type = "pwa-node",
		request = "launch",
		name = "Debug Jest Tests",
		-- trace = true, -- include debugger info
		runtimeExecutable = "node",
		runtimeArgs = {
		  "./node_modules/jest/bin/jest.js",
		  "--runInBand",
		},
		rootPath = "${workspaceFolder}",
		cwd = "${workspaceFolder}",
		console = "integratedTerminal",
		internalConsoleOptions = "neverOpen"
	  },
	  {
		name = "Chrome (9222)",
		type = "chrome",
		request = "attach",
		program = "${file}",
		cwd = vim.fn.getcwd(),
		sourceMaps = true,
		protocol = "inspector",
		port = 9222,
		webRoot = "${workspaceFolder}",
	  },
  }
end

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end

require('Comment').setup({
	toggler = {
		line = '<leader>cc',
		block = '<leader>bc',
	},
	opleader = {
		line= '<leader>c',
		block = '<leader>b',
	}
})

-- Setup telescope.
local actions = require("telescope.actions")
local lga_actions = require("telescope-live-grep-args.actions")

require('telescope').setup({
	defaults = {
		file_ignore_patterns = {
			"%.git/",
			"%.venv/",
			"node_modules",
		},
		mappgings = {
			i = {
				["<C-x>"] = false,
				["<C-/>"] = "which_key",
				["<C-q>"] = actions.send_to_qflist,
			},
		},
		vim_grep_arguments = {
		  "rg",
		  "--color=never",
		  "--no-heading",
		  "--with-filename",
		  "--line-number",
		  "--column",
		  "--smart-case",
		  "--hidden"
		},
	},
	pickers = {
		find_files = {
			find_command = {
				"fd",
				"--type",
				"f",
				"--strip-cwd-prefix",
				"--no-ignore-vcs",
				"--color=never",
				"--hidden",
				"--follow"
			}
		},
	},
	extensions = {
		fzf = {
			fuzzy = true,
			override_generic_sorter = true,
			override_file_sorter = true,
		},
		live_grep_args = {
			auto_quoting = true,
			mappings = {
				i = {
					["<C-k>"] = lga_actions.quote_prompt({ postfix = ' -t'}),
				}
			}
		}
	},
})

require("telescope").load_extension("fzf")
require("telescope").load_extension('harpoon')
require("telescope").load_extension("git_worktree")
require("telescope").load_extension("dap")

-- Setup nvim-cmp.
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
local cmp = require("cmp")

cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done({ map_char = { tex = '' } }))

local lspkind = require("lspkind")

local source_mapping = {
	buffer = "[Buffer]",
	nvim_lsp = "[LSP]",
	nvim_lua = "[Lua]",
	cmp_tabnine = "[TN]",
	path = "[Path]",
}

cmp.setup({
	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body) -- for `luasnip` users
		end,
	},
	mapping = cmp.mapping.preset.insert({
		['<C-u>'] = cmp.mapping.scroll_docs(-4),
		['<C-d>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-y>'] = cmp.mapping.confirm({ select = true }),
		['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
	}),
	formatting = {
		format = function(entry, vim_item)
			vim_item.kind = lspkind.presets.default[vim_item.kind]
			local menu = source_mapping[entry.source.name]
			if entry.source.name == 'cmp_tabnine' then
				if entry.completion_item.data ~= nil and entry.completion_item.data.detail ~= nil then
					menu = entry.completion_item.data.detail .. ' ' .. menu
				end
				vim_item.kind = ''
			end
			vim_item.menu = menu
			return vim_item
		end

	},
	sources = {
		{ name = "cmp_tabnine" },
		{ name = "nvim_lsp" },
		{ name = "nvim_lsp_signature_help" },
		{ name = "luasnip" },
		{ name = "buffer" },
	},
})

local tabnine = require('cmp_tabnine.config')
tabnine:setup({
	max_lines = 1000;
	max_num_results = 20;
	sort = true;
	run_on_every_keystroke = true;
	snippet_placeholder = '..';
	ignored_file_types = { -- default is not to ignore
		-- uncomment to ignore in lua:
		-- lua = true
	};
})

local rt = require("rust-tools")

rt.setup({
  server = {
	on_attach = function(_, bufnr)
	  -- Hover actions
	  vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
	  -- Code action groups
	  vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
	end,
  },
})

local servers = {
		"awk_ls",
		"bashls",
		"cssls",
		"dockerls",
		"eslint",
		"gopls",
		"html",
		"jsonls",
		"lemminx",
		"tsserver",
		"jedi_language_server",
		"jsonnet_ls",
		"rust_analyzer",
		"terraformls",
		"tflint",
		"vimls",
		"yamlls",
		"marksman"
}

require("mason").setup {
	-- #needed for vpn :h mason-provider-errors
	-- providers = {
	--	   "mason.providers.client",
	--	   "mason.providers.registry-api",
	-- }
}
require("mason-lspconfig").setup({ ensure_installed = servers })
require("mason-nvim-dap").setup({
	ensure_installed = {"delve", "chrome-debug-adapter", "js-debug-adapter"}
})

local lspconfig = require('lspconfig')

-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)


local opts = {noremap=true, silent=true}

local on_attach = function(client, bufnr)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gh', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gsh', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gdt', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'grn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

--nnoremap <leader>gd :lua vim.lsp.buf.definition()<CR>
--nnoremap <leader>gi :lua vim.lsp.buf.implementation()<CR>
--nnoremap <leader>gsh :lua vim.lsp.buf.signature_help()<CR>
--nnoremap <leader>grr :lua vim.lsp.buf.references()<CR>
--nnoremap <leader>grn :lua vim.lsp.buf.rename()<CR>
--nnoremap <leader>gh :lua vim.lsp.buf.hover()<CR>
--nnoremap <leader>gca :lua vim.lsp.buf.code_action()<CR>
--nnoremap <leader>gsd :lua vim.lsp.diagnostic.show_line_diagnostics(); vim.lsp.util.show_line_diagnostics()<CR>
--nnoremap <leader>gp :lua vim.lsp.diagnostic.goto_prev()<CR>
--nnoremap <leader>gn :lua vim.lsp.diagnostic.goto_next()<CR>
--nnoremap <leader>gq :lua vim.lsp.diagnostic.setloclist()<CR>
--nnoremap <leader>gll :call LspLocationList()<CR>

end

for _, lsp in pairs(servers) do
	lspconfig[lsp].setup {
		capabilities = capabilities,
		on_attach = on_attach,
		flags = {
			-- This will be the default in neovim 0.7+
			debounce_text_changes = 150,
		},
		settings = {
			yaml = {
				customTags = { '!reference sequence' },
				schemas = {
					["https://smocker.dev/smocker.schema.json"] = "/**/*mock*.yml",
					["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
					["https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json"] = "/**/*.gitlab-ci.yml",
					["https://squidfunk.github.io/mkdocs-material/schema.json"] = "mkdocs.yml"
				}
			},
			gopls = {
				buildFlags = {"-tags=testing,integration"}
			}
		}
	}
end

require('sonarlint').setup({
   server = {
      cmd = { 
         'sonarlint-language-server',
         -- Ensure that sonarlint-language-server uses stdio channel
         '-stdio',
         '-analyzers',
         -- paths to the analyzers you need, using those for python and java in this example
         vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarpython.jar"),
         -- vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarcfamily.jar"),
         vim.fn.expand("$MASON/share/sonarlint-analyzers/sonargo.jar"),
         vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarhtml.jar"),
         vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarjs.jar"),
         vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarxml.jar"),
         vim.fn.expand("$MASON/share/sonarlint-analyzers/sonartext.jar"),
         -- vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarjava.jar"),
      }
   },
   filetypes = {
      -- Tested and working
      'python',
      -- 'cpp',
	  'go',
	  'js',
	  'ts',
	  'xml',
	  'html',
      -- Requires nvim-jdtls, otherwise an error message will be printed
      -- 'java',
   }
})


local snippets_paths = function()
	local plugins = { "friendly-snippets" }
	local paths = {}
	local path
	local root_path = vim.env.HOME .. "/.vim/plugged/"
	for _, plug in ipairs(plugins) do
		path = root_path .. plug
		if vim.fn.isdirectory(path) ~= 0 then
			table.insert(paths, path)
		end
	end
	return paths
end

require("luasnip.loaders.from_vscode").lazy_load({
	paths = snippets_paths(),
	include = nil, -- Load all languages
	exclude = {},
})
