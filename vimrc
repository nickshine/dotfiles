" DEPRECATED - using neovim init.vim
set exrc
set guicursor=
set relativenumber
set number
set nohlsearch
set hidden
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nowrap
set ignorecase
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect
set colorcolumn=80
set signcolumn=yes
set cmdheight=1
set updatetime=750


" set termguicolors
highlight Normal guibg=NONE
set ruler
" set title
set autoindent
set preserveindent
set smarttab
set autoread
set showmatch
set nocompatible
set backspace=indent,eol,start
set laststatus=2
set pastetoggle=<F3>
"
" set clipboard=unnamed
"
nnoremap Y yg$
" copy to system clipboard
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nmap <leader>Y "+y

" omnicomplete:
filetype plugin indent on
filetype indent on
set omnifunc=syntaxcomplete#Complete
" imap <Tab> <C-P>
set wildmode=longest,list:longest
" inoremap <C-c> <C-x><C-o>
" inoremap <C-@> <C-Space>

set listchars=tab:»\ ,extends:❯,precedes:❮,nbsp:·,trail:·

set foldmethod=indent
set foldlevelstart=20
set foldnestmax=10
"set nofoldenable
" use comma to open/close folds
nnoremap , za
vnoremap , za

autocmd FileType puppet setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
autocmd FileType vue setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
autocmd FileType css setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
autocmd FileType java setlocal shiftwidth=4 tabstop=4 softtabstop=4 expandtab
autocmd BufNewFile,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufRead *.md setlocal textwidth=100

let mapleader = " "
nnoremap <Leader>l :set list!<CR>
nnoremap <Leader>h :set hlsearch!<CR>
nmap <Leader>sr :source $MYVIMRC<CR>
nmap <Leader>vrc :tabedit $MYVIMRC<CR>

nnoremap - :vertical res-5<CR>
nnoremap = :vertical res+5<CR>
nnoremap _ :res-5<CR>
nnoremap + :res+5<CR>


" select all visual
map <Leader>sa ggVG

" Delete without yanking
map <Leader>da "_d

" Full Screen Window
nmap <Leader>fw <C-W>_

nnoremap <Leader>1 1gt
nnoremap <Leader>2 2gt
nnoremap <Leader>3 3gt
nnoremap <Leader>4 4gt
nnoremap <Leader>5 5gt
nnoremap <Leader>6 6gt
nnoremap <Leader>7 7gt
nnoremap <Leader>8 8gt
nnoremap <Leader>9 9gt

"Automatic Closing Brackets and Parenthesis
"inoremap ( ()<Esc>i
"inoremap { {<CR><BS>}<Esc>ko

runtime macros/matchit.vim

" ***************************** plugin settings *****************************

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

	" session restore
	Plug 'tpope/vim-obsession'

	" autoread open file changes
	Plug 'tmux-plugins/vim-tmux-focus-events'

	" navigation
	Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': { -> fzf#install() } }
	Plug 'junegunn/fzf.vim'
	Plug 'christoomey/vim-tmux-navigator'
	Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
	Plug 'jeffkreeftmeijer/vim-numbertoggle'
	Plug 'mbbill/undotree'

	" commenting
	Plug 'scrooloose/nerdcommenter'

	" language/syntax support
	Plug 'tpope/vim-fugitive'
	Plug 'editorconfig/editorconfig-vim'
	Plug 'jiangmiao/auto-pairs'
	Plug 'dense-analysis/ale'
	Plug 'hdima/python-syntax'
	Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
	Plug 'ternjs/tern_for_vim', { 'for': 'javascript' }
	Plug 'posva/vim-vue', { 'for': 'vue' }
	Plug 'hashivim/vim-terraform', { 'for': 'terraform' }
	" Plug 'mxw/vim-jsx'
	Plug 'rodjek/vim-puppet', { 'for': 'puppet' }
	Plug 'nelstrom/vim-markdown-folding', { 'for': 'markdown' }
	Plug 'tpope/vim-jdaddy'
	Plug 'towolf/vim-helm'
	Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
	Plug 'cespare/vim-toml', { 'for': 'toml' }
	" Plug 'neoclide/coc.nvim', {'branch': 'release'}

	" theme
	Plug 'chriskempson/base16-vim'
	Plug 'itchyny/lightline.vim'
	Plug 'daviesjamie/vim-base16-lightline'
	Plug 'edkolev/tmuxline.vim'
  Plug 'kyazdani42/nvim-web-devicons'



	" Plug 'anned20/vimsence'
	" Plug 'vbe0201/vimdiscord'
	" Plug 'Akulen/vim-dcrpc'
	Plug 'mileszs/ack.vim'

call plug#end()

" nerdcommenter settings
let g:NERDSpaceDelims = 1
let g:NERDTrimTrailingWhitespace = 1

"let g:NERDTreeNodeDelimiter = "\u00a0"

" vim-go directives
let g:go_highlight_fields = 1

au filetype go inoremap <buffer> . .<C-x><C-o>


"FZF
nnoremap <C-F> :FZF<CR>

nmap <Leader>nt :NERDTreeToggle<CR>

if filereadable(expand("~/.vimrc_background"))
	 let base16colorspace=256
	 source ~/.vimrc_background
endif

" lightline theme
let g:lightline = { 'colorscheme': 'base16' }
" let g:tmuxline_powerline_separators = 0


let g:ale_lint_on_text_changed = 'never'
let g:ale_linters = {
				\ 'javascript': ['eslint'],
				\ 'go': ['gopls'],
				\ 'python': ['flake8', 'pylint'],
				\}

let python_highlight_all=1

let g:javascript_plugin_jsdoc = 1

" tern js
let g:tern_show_argument_hints='on_hold'
let g:tern_map_keys=1
nmap <Leader>d :TernDef<CR>
nmap <Leader>r :TernRename<CR>
nmap <Leader>t :TernType<CR>
nmap <Leader>a :TernRefs<CR>

com! FormatJSON %!python -m json.tool
com! FormatXML %!xmllint --format %

" packloadall
silent! helptags ALL
