# kmonad


## Setup

1. Add `/bin/launchctl` and `~/.local/bin/kmonad` binaries to `System Preferences` > `Security & Privacy` > `Privacy` >
  `Input Monitoring`
2. Link plist in to `/Library/LaunchDaemons/`
3.

    ```bash
    sudo ln -sf ~/.config/kmonad/kmonad.plist /Library/LaunchDaemons/local.kmonad.plist
    sudo chown root:wheel /Library/LaunchDaemons/local.kmonad.plist
    ```

4. Load kmonad

    ```bash
    sudo launchctl load /Library/LaunchDaemons/local.kmonad.plist
    ```



* https://github.com/kmonad/kmonad/issues/105

